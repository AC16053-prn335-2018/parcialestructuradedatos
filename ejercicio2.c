#include <stdio.h>
#include <stdlib.h>


void main() {
    int size, i, j, sumas = 0, sumaFilas = 0, sumaColumnas = 0, sumadiagonal = 0, fil=0, col=1, sumaTot=0;

    printf("Ingrese el tamaño de la matriz cuadrada (nxn):\n");
    scanf("%d", &size);
    
    int m[size][size];

    printf("\nIngrese valores a la matriz:\n");
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            printf("Posicion(%d,%d): ", i + 1, j + 1);
            scanf("%d", &m[i][j]);
        }
    }
    
//Imprimir matriz
    for (i = 0; i < size; i++) {
        printf("[");
        for (j = 0; j < size; j++) {
            printf(" %d ", m[i][j]);
        }
        printf("]\n");
    }

    j=0;
    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            sumas = sumas + m[i][j];
        }
        sumaTot=sumas;
        sumas=0;
        break;
    }

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            sumas = sumas + m[i][j];
        }
        if (sumas == sumaTot) {
            sumaFilas++;
            sumas = 0;
        }
    }

    for (i = 0; i < size; i++) {
        for (j = 0; j < size; j++) {
            sumas = sumas + m[j][i];
        }
        if (sumas ==sumaTot) {
            sumaColumnas++;
            sumas = 0;
        }
    }  

    for (fil = 0; fil < size; fil++) {
        sumas=sumas+m[fil][size-col];
        col=col+1;
    }
    if (sumas==sumaTot) {
        sumadiagonal++;
    }
    col = 0;
    sumas = 0;   

    for (fil = 0; fil < size; fil++) {
        sumas = sumas + m[fil][col];
        col = col + 1;
    } 
    if (sumas==sumaTot) {
        sumadiagonal++;
    }

    if (sumaFilas==size && sumaColumnas==size && sumadiagonal==2) {
        printf("\nEs una matriz amiga :) ");

    }else{
        printf("\nNo es una matriz amiga :( ");
    }
}

