#include <stdio.h>
#include <stdlib.h>

void main() {
    int filas, columnas, fil, col;
    int hallarC = 0, hallarF = 0, actual, actualFila, actualColumna;
    printf("Ingrese el tamaño de las filas para la matriz\n");
    scanf("%d", &filas);

    printf("Ingrese el tamaño de las columnas para la matriz\n");
    scanf("%d", &columnas);

    int m[filas][columnas];

    //Ingreso de datos a la matriz
    for (fil = 0; fil < filas; fil++) {
        for (col = 0; col < columnas; col++) {
            printf("Ingrese un  valor para la posición (%d,%d)\n", fil + 1, col + 1);
            scanf("%d", &m[fil][col]);
        }
    }

    //Imprimir matriz
    for (fil = 0; fil < filas; fil++) {
        printf("\n");
        for (col = 0; col < columnas; col++) {
            printf("%d\t", m[fil][col]);
        }
    }

    //Verificar si es el menor en la columna y fila
    for (fil = 0; fil < filas; fil++) {
        for (col = 0; col < columnas; col++) {
            //Asigno el valor de la matriz a una variable
            actual = m[fil][col];
            //Me recorre toda las columnas (se mueve de izquierda a la derecha)
            for (actualColumna = 0; actualColumna < columnas; actualColumna++) {
                //Verifica si es el menor en su fila      
                if (actual > m[fil][actualColumna]) {
                    hallarC=hallarC+1;
                }
            }
            //Me recorre las filas (de arriba para abajo)
            for (actualFila = 0; actualFila < filas; actualFila++) {
                //Verifica si es el mayor en su columna
                if (actual < m[actualFila][col]) {
                    hallarF=hallarF+1;
                }
            }
            if (hallarC == 0 && hallarF == 0) {
                printf("\nSe ha encontrado un punto de silla en la matriz, y es: %d, que se encuentra en la fila: %d y la columna: %d\n", actual, (fil + 1), (col + 1));
            } 
            hallarF=0;
            hallarC=0;
        }
    }
}
